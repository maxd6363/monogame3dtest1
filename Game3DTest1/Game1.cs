﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Game3DTest1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;

        private IList<GameObject> obj = new List<GameObject>();

        private IList<Terrain> listChunck = new List<Terrain>();

        private Random random = new Random();
        private Camera camera;


        private static readonly int WINDOW_WIDTH = 1000;
        private static readonly int WINDOW_HEIGHT = 600;

        private static readonly int WINDOW_WIDTH_FULLSCREEN = 1600;
        private static readonly int WINDOW_HEIGHT_FULLSCREEN = 900;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Window.Title = "BAPPS!";
            IsMouseVisible = true;


            base.Initialize();

            //graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH_FULLSCREEN + 80;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT_FULLSCREEN + 150;
            graphics.ApplyChanges();


        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            /*
            for (int i = 0; i < 24; i++)
            {
                for (int j = 0; j < 24; j++)
                {

                    listChunck.Add(
                        new Terrain(
                            new Vector3((tabSize - 1) * 10f * i / 2f, (tabSize - 1) * 10f * j / 2f, 0),
                            10,
                            3,
                            16,
                            graphics
                            )

                        );
                }   
            }
            */


            listChunck.Add(
                      new Terrain(
                            Vector3.Zero,
                            100,
                            500,
                            150,
                            graphics
                            )
                      );


            /*
            List<Model> listModel = new List<Model>
            {
                Content.Load<Model>("Oak_Tree"),
                Content.Load<Model>("Fir_Tree"),
                Content.Load<Model>("Palm_Tree"),
                Content.Load<Model>("Poplar_Tree")
            };
            



            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    float xrand = (float)(random.NextDouble() * 4.0f - 2);
                    float yrand = (float)(random.NextDouble() * 4.0f - 2);

                    obj.Add(new Tree(listModel[random.Next(0, listModel.Count)], new Vector3((i + xrand) * space, (j + yrand) * space, random.Next(0, 10) / 10.0f)));

                }
            }

            */
            camera = new Camera(new Vector3(-50, 0, 0), GraphicsDevice, 120, 1, 30000, 0.5f);

        }






        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.F11))
            {
                graphics.PreferredBackBufferWidth = WINDOW_WIDTH_FULLSCREEN;
                graphics.PreferredBackBufferHeight = WINDOW_HEIGHT_FULLSCREEN;
                graphics.ToggleFullScreen();
                graphics.ApplyChanges();
            }


            /*
            foreach (GameObject o in obj)
            {
                o.Update();
            }*/


            //UpdateTab();
            //terrain.Update(Vector3.Zero,10, tab, tabSize);

            camera.Update();


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(149, 165, 166));
            /*
            foreach (GameObject o in obj)
            {
                o.Draw(gameTime, graphics, camera);
            }
            */

            foreach (Terrain o in listChunck)
            {
                o.Draw(gameTime, graphics, camera);
            }


            base.Draw(gameTime);
        }



    }
}
