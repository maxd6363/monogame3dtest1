﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game3DTest1
{
    public class Ground
    {
        private VertexPositionColor[] floor;



        private BasicEffect effect;
        public Vector3 Position { get; private set; }

        private GraphicsDeviceManager graphics;


        public Ground(Vector3 position, float sizeX, float sizeY, GraphicsDeviceManager graphics)
        {
            Position = position;
            Init(position, sizeX, sizeY);
            effect = new BasicEffect(graphics.GraphicsDevice);
            effect.VertexColorEnabled = true;
            effect.LightingEnabled = false;
            this.graphics = graphics;


        }

        private void Init(Vector3 pos, float sizeX, float sizeY)
        {
            Color color = Color.ForestGreen;
            floor = new VertexPositionColor[6];
            floor[0] = new VertexPositionColor(pos, color);
            floor[1] = new VertexPositionColor(pos + new Vector3(0, sizeY, 0), color);
            floor[2] = new VertexPositionColor(pos + new Vector3(sizeX, 0, 0), color);
            floor[3] = new VertexPositionColor(floor[1].Position, color);
            floor[4] = new VertexPositionColor(pos + new Vector3(sizeX, sizeY, 0), color);
            floor[5] = new VertexPositionColor(floor[2].Position, color);

        }

        public void Draw(GameTime gameTime, GraphicsDeviceManager graphics, Camera camera)
        {
            effect.World = Matrix.CreateTranslation(Position);
            effect.View = Matrix.CreateLookAt(camera.Position, camera.Target, camera.UpDirection);
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(camera.Fov, camera.AspectRatio, camera.NearCut, camera.FarCut);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, floor, 0, 2);
            }

        }

    }
}
