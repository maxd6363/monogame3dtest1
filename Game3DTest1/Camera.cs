﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game3DTest1
{
    public class Camera
    {
        public Vector3 Position;
        public Vector3 Target;
        public Vector3 UpDirection;

        public Matrix ViewMatrix
        {
            get
            {
                return Matrix.CreateLookAt(Position, Target, UpDirection);

            }
            set
            {
                ViewMatrix = value;
            }
        }
        public Matrix ProjectionMatrix
        {
            get
            {
                return Matrix.CreatePerspectiveFieldOfView(Fov, AspectRatio, NearCut, FarCut);
            }
            set
            {
                ProjectionMatrix = value;
            }
        }
        public Matrix WorldMatrix
        {
            get
            {
                return Matrix.CreateTranslation(Position);
            }
            set
            {
                WorldMatrix = value;
            }
        }






        public float AspectRatio;

        public float NearCut;
        public float FarCut;
        public float CameraSpeed;

        private float cameraSpeedSprint;
        private float cameraSpeedNormal;


        private float fovSprint;
        private float fovNormal;


        private float fov;

        public float Fov
        {
            get { return MathHelper.ToRadians(fov); }
            set
            {
                if (value <= 0)
                {
                    value = 1;
                }
                if (value >= 180)
                {
                    value = 179;
                }
                fov = value;
            }
        }


        public GraphicsDevice GraphicsDevice;

        private Random random = new Random();


        public Camera(Vector3 position, GraphicsDevice graphics, float fov, float nearView, float farView, float cameraSpeed)
        {
            Position = position;
            Target = position + new Vector3(50, 0, 0);
            GraphicsDevice = graphics;
            AspectRatio = graphics.DisplayMode.AspectRatio;
            Fov = fov;
            fovNormal = fov;
            fovSprint = fov * 1.2f;


            NearCut = nearView;
            FarCut = farView;
            CameraSpeed = cameraSpeed;
            cameraSpeedNormal = cameraSpeed;
            cameraSpeedSprint = cameraSpeed * 50f;

        UpDirection = Vector3.UnitZ;

        }


        public void Update()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                Position += new Vector3(1, 0, 0) * CameraSpeed;
                Target += new Vector3(1, 0, 0) * CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                Position += new Vector3(-1, 0, 0) * CameraSpeed;
                Target += new Vector3(-1, 0, 0) * CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                Position += new Vector3(0, 1, 0) * CameraSpeed;
                Target += new Vector3(0, 1, 0) * CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Position += new Vector3(0, -1, 0) * CameraSpeed;
                Target += new Vector3(0, -1, 0) * CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                Position += new Vector3(0, 0, 1) * CameraSpeed;
                Target += new Vector3(0, 0, 1) * CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                Position += new Vector3(0, 0, -1) * CameraSpeed;
                Target += new Vector3(0, 0, -1) * CameraSpeed;
            }


            if (Keyboard.GetState().IsKeyDown(Keys.J))
            {
                Matrix rota = Matrix.CreateRotationZ(MathHelper.ToRadians(5f));
                Target = Vector3.Transform(Target - Position, rota);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.L))
            {
                Matrix rota = Matrix.CreateRotationZ(MathHelper.ToRadians(-5f));
                Target = Vector3.Transform(Target - Position, rota);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.I))
            {
                Matrix rota = Matrix.CreateRotationX(MathHelper.ToRadians(5f));
                Target = Vector3.Transform(Target - Position, rota);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.K))
            {
                Matrix rota = Matrix.CreateRotationX(MathHelper.ToRadians(-5f));
                Target = Vector3.Transform(Target - Position, rota);
            }





            /*FOV*/
            if (Keyboard.GetState().IsKeyDown(Keys.P))
            {
                Fov = fov + 1;//avoid using radian value
            }
            if (Keyboard.GetState().IsKeyDown(Keys.M))
            {
                Fov = fov - 1;
            }



            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            {
                CameraSpeed = cameraSpeedSprint;
                Fov = fovSprint;
            }
            if (Keyboard.GetState().IsKeyUp(Keys.LeftShift))
            {
                CameraSpeed = cameraSpeedNormal;
                Fov = fovNormal;

            }


        }



        public Matrix getView()
        {
            return Matrix.CreateLookAt(Position, Target, UpDirection);
        }
        public Matrix getProjection()
        {
            return Matrix.CreatePerspectiveFieldOfView(Fov, AspectRatio, NearCut, FarCut);
        }
        public Matrix getWorld()
        {
            return Matrix.CreateTranslation(Position);
        }


    }
}
