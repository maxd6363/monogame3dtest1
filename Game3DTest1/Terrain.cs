﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game3DTest1
{
    public class Terrain
    {

        public Vector3 Position { get; private set; }
        public float ScaleXY { get; private set; }
        public float ScaleZ { get; private set; }
        public float[,] TabHeight { get; private set; }
        public int ChunckSize { get; private set; }
        public int NumberVertex { get; private set; }



        private Random random = new Random();
        private VertexPositionColor[] floor;
        private BasicEffect basicEffect;
        private GraphicsDeviceManager graphicsDeviceManager;





        public Terrain(Vector3 position, float scaleXY, float scaleZ, float[,] tab, int chunckSize, GraphicsDeviceManager graphics)
        {
            basicEffect = new BasicEffect(graphics.GraphicsDevice)
            {
                VertexColorEnabled = true,
                LightingEnabled = false
            };

            Position = position;
            ScaleXY = scaleXY;
            ScaleZ = scaleZ;
            ChunckSize = chunckSize;
            graphicsDeviceManager = graphics;
            TabHeight = tab;
            NumberVertex = (chunckSize - 1) * (chunckSize - 1) * 2 * 3;

            Init();



        }


        public Terrain(Vector3 position, float scaleXY, float scaleZ, int chunckSize, GraphicsDeviceManager graphics)
        {
            basicEffect = new BasicEffect(graphics.GraphicsDevice)
            {
                VertexColorEnabled = true,
                LightingEnabled = false
            };

            Position = position;
            ScaleXY = scaleXY;
            ScaleZ = scaleZ;
            ChunckSize = chunckSize;
            graphicsDeviceManager = graphics;
            NumberVertex = (chunckSize - 1) * (chunckSize - 1) * 2 * 3;

            StubTab();
            Init();
        }



        public void Update(Vector3 position, float scaleXY, float scaleZ, float[,] tab, int chunckSize)
        {

            Position = position;
            TabHeight = tab;
            ScaleXY = scaleXY;
            ScaleZ = scaleZ;
            NumberVertex = (chunckSize - 1) * (chunckSize - 1) * 2 * 3;

            Init();

        }


        private void Init()
        {
            int i = 0;
            floor = new VertexPositionColor[NumberVertex];


            for (int x = 0; x < ChunckSize - 1; x++)
            {
                for (int y = 0; y < ChunckSize - 1; y++)
                {


                    // first traingle of the square
                    floor[i + 0] = new VertexPositionColor(Position + new Vector3((x + 1) * ScaleXY, (y + 0) * ScaleXY, TabHeight[x + 1, y + 0] * ScaleZ), HeightToColor(TabHeight[x + 1, y + 0]));
                    floor[i + 1] = new VertexPositionColor(Position + new Vector3((x + 0) * ScaleXY, (y + 0) * ScaleXY, TabHeight[x + 0, y + 0] * ScaleZ), HeightToColor(TabHeight[x + 0, y + 0]));
                    floor[i + 2] = new VertexPositionColor(Position + new Vector3((x + 0) * ScaleXY, (y + 1) * ScaleXY, TabHeight[x + 0, y + 1] * ScaleZ), HeightToColor(TabHeight[x + 0, y + 1]));

                    // second triangle of the square
                    floor[i + 3] = new VertexPositionColor(Position + new Vector3((x + 1) * ScaleXY, (y + 0) * ScaleXY, TabHeight[x + 1, y + 0] * ScaleZ), HeightToColor(TabHeight[x + 1, y + 0]));
                    floor[i + 4] = new VertexPositionColor(Position + new Vector3((x + 0) * ScaleXY, (y + 1) * ScaleXY, TabHeight[x + 0, y + 1] * ScaleZ), HeightToColor(TabHeight[x + 0, y + 1]));
                    floor[i + 5] = new VertexPositionColor(Position + new Vector3((x + 1) * ScaleXY, (y + 1) * ScaleXY, TabHeight[x + 1, y + 1] * ScaleZ), HeightToColor(TabHeight[x + 1, y + 1]));


                    i += 6;
                    // /!\/!\/!\/!\/!\ culling can mess it up

                }

            }

        }

        /// <summary>
        /// return color corespondig to height
        /// </summary>
        /// <param name="height">between 0 and 1</param>
        /// <returns></returns>
        private Color HeightToColor(float height)
        {
            Color[] colors = new Color[6];
            colors[0] = new Color(12, 0, 117);      //deep ocean
            colors[1] = new Color(8, 24, 255);      //ocean
            colors[2] = new Color(255, 229, 97);    //beach
            colors[3] = new Color(31, 133, 25);     //plain
            colors[4] = new Color(94, 62, 22);      //mountain
            colors[5] = new Color(207, 198, 188);   //ice

            if (height > 0.0 && height < 0.2) return colors[0];
            if (height > 0.2 && height < 0.4) return colors[1];
            if (height > 0.4 && height < 0.6) return colors[2];
            if (height > 0.6 && height < 0.8) return colors[3];
            if (height > 0.8 && height < 1.0) return colors[4];
            return Color.White;

        }




        private void StubTab()
        {
            TabHeight = new float[ChunckSize, ChunckSize];
            for (int i = 0; i < ChunckSize; i++)
            {
                for (int j = 0; j < ChunckSize; j++)
                {
                    TabHeight[i, j] = (float)(random.NextDouble());

                }
            }
        }



        public void Draw(GameTime gameTime, GraphicsDeviceManager graphics, Camera camera)
        {
            basicEffect.World = Matrix.CreateTranslation(Position);
            basicEffect.View = Matrix.CreateLookAt(camera.Position, camera.Target, camera.UpDirection);
            basicEffect.Projection = Matrix.CreatePerspectiveFieldOfView(camera.Fov, camera.AspectRatio, camera.NearCut, camera.FarCut);
            //effect.EnableDefaultLighting();


            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, floor, 0, NumberVertex / 3);
            }

        }

        public override string ToString()
        {
            return $"Position : {Position}\nScaleXY : {ScaleXY}\nScaleZ : {ScaleZ}\nChunckSize : {ChunckSize}\nNumberOfVertex : {NumberVertex}\n";
        }


    }

}
