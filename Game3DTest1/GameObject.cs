﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game3DTest1
{
    public class GameObject
    {


        public Vector3 Position { get; set; }
        public Model Model { get; private set; }



        public GameObject(Model model, Vector3 position)
        {
            Model = model;
            Position = position;
        }

        public virtual void Update()
        {

        }
        public virtual void Draw(GameTime gameTime, GraphicsDeviceManager graphics, Camera camera)
        {
            foreach (ModelMesh mesh in Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.PreferPerPixelLighting = true;

                    effect.View = Matrix.CreateLookAt(camera.Position, camera.Target, camera.UpDirection);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(camera.Fov, camera.AspectRatio, camera.NearCut, camera.FarCut);
                    effect.World = Matrix.CreateWorld(Position,Vector3.Forward,Vector3.Up);
                    
                    /*
                    effect.View = camera.ViewMatrix;
                    effect.Projection = camera.ProjectionMatrix;
                    effect.World = camera.WorldMatrix;
                    */

                    /*
                    effect.View = camera.getView();
                    effect.Projection = camera.getProjection();
                    effect.World = camera.getWorld();
                    */
                    /*NOT WORKING FOR NO REASON*/






                }
                mesh.Draw();

            }
        }


    }
}
