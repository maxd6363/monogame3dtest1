﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game3DTest1
{
    class Tree : GameObject
    {
        public Tree(Model model, Vector3 position) : base(model, position)
        {


        }

        public override void Draw(GameTime gameTime, GraphicsDeviceManager graphics, Camera camera)
        {
            base.Draw(gameTime, graphics, camera);
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
